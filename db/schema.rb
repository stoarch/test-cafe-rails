# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170210134705) do

  create_table "c_tables", force: :cascade do |t|
    t.integer  "number",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "fullname",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "drink_dict_items", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "drink_dicts", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "drinks", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.decimal  "price",                          precision: 10
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "lineitem_id",        limit: 4
    t.integer  "drink_dict_item_id", limit: 4
    t.integer  "count",              limit: 4
  end

  add_index "drinks", ["drink_dict_item_id"], name: "index_drinks_on_drink_dict_item_id", using: :btree
  add_index "drinks", ["lineitem_id"], name: "index_drinks_on_lineitem_id", using: :btree

  create_table "food_dict_items", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "food_dicts", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.decimal  "price",                  precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "foods", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.decimal  "price",                         precision: 10
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "lineitem_id",       limit: 4
    t.integer  "food_dict_item_id", limit: 4
    t.integer  "count",             limit: 4
  end

  add_index "foods", ["food_dict_item_id"], name: "index_foods_on_food_dict_item_id", using: :btree
  add_index "foods", ["lineitem_id"], name: "index_foods_on_lineitem_id", using: :btree

  create_table "lineitems", force: :cascade do |t|
    t.integer  "number",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "order_id",   limit: 4
    t.integer  "kind",       limit: 4
  end

  add_index "lineitems", ["order_id"], name: "index_lineitems_on_order_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "number",      limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "waitress_id", limit: 4
  end

  add_index "orders", ["waitress_id"], name: "index_orders_on_waitress_id", using: :btree

  create_table "waitresses", force: :cascade do |t|
    t.string   "fullname",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "drinks", "drink_dict_items"
  add_foreign_key "drinks", "lineitems"
  add_foreign_key "foods", "food_dict_items"
  add_foreign_key "foods", "lineitems"
  add_foreign_key "lineitems", "orders"
  add_foreign_key "orders", "waitresses"
end
