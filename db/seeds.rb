CTable.delete_all

CTable.create!(number:1)
CTable.create!(number:2)
CTable.create!(number:3)
CTable.create!(number:4)
CTable.create!(number:6)

Client.delete_all

Client.create!(fullname: 'Client A')
Client.create!(fullname: 'Client B')
Client.create!(fullname: 'Client C')
Client.create!(fullname: 'Client D')
Client.create!(fullname: 'Client E')

Drink.delete_all

Drink.create!( title: 'Drink1', price: 10.01 )
Drink.create!( title: 'Drink2', price: 20.02 )
Drink.create!( title: 'Drink3', price: 30.03 )
Drink.create!( title: 'Drink4', price: 40.04 )

Food.delete_all

Food.create!( title: 'Food1', price: 5.05 )
Food.create!( title: 'Food2', price: 15.15 )
Food.create!( title: 'Food3', price: 25.25 )
Food.create!( title: 'Food4', price: 35.25 )

Order.delete_all

Waitress.delete_all

Waitress.create!( fullname: 'Waitress A' )
Waitress.create!( fullname: 'Waitress B' )
Waitress.create!( fullname: 'Waitress C' )
Waitress.create!( fullname: 'Waitress D' )
Waitress.create!( fullname: 'Waitress E' )
Waitress.create!( fullname: 'Waitress F' )
