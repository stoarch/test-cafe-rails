class AddRefsToDrink < ActiveRecord::Migration
  def change
    add_reference :drinks, :drink_dict_item, index: true, foreign_key: true
  end
end
