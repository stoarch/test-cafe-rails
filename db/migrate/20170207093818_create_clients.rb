class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :fullname

      t.timestamps null: false
    end
  end
end
