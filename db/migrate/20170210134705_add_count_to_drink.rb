class AddCountToDrink < ActiveRecord::Migration
  def change
    add_column :drinks, :count, :integer
  end
end
