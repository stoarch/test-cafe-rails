class AddCountToFood < ActiveRecord::Migration
  def change
    add_column :foods, :count, :integer
  end
end
