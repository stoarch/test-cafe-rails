class CreateCTables < ActiveRecord::Migration
  def change
    create_table :c_tables do |t|
      t.integer :number

      t.timestamps null: false
    end
  end
end
