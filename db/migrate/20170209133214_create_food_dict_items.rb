class CreateFoodDictItems < ActiveRecord::Migration
  def change
    create_table :food_dict_items do |t|
      t.string :title
      t.decimal :price

      t.timestamps null: false
    end
  end
end
