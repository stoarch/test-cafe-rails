class CreateDrinks < ActiveRecord::Migration
  def change
    create_table :drinks do |t|
      t.string :title
      t.decimal :price

      t.timestamps null: false
    end
  end
end
