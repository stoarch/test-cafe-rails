class CreateWaitresses < ActiveRecord::Migration
  def change
    create_table :waitresses do |t|
      t.string :fullname

      t.timestamps null: false
    end
  end
end
