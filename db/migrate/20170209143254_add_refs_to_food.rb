class AddRefsToFood < ActiveRecord::Migration
  def change
    add_reference :foods, :food_dict_item, index: true, foreign_key: true
  end
end
