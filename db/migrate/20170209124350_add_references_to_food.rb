class AddReferencesToFood < ActiveRecord::Migration
  def change
    add_reference :foods, :lineitem, index: true, foreign_key: true
  end
end
