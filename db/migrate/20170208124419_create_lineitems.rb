class CreateLineitems < ActiveRecord::Migration
  def change
    create_table :lineitems do |t|
      t.integer :number

      t.timestamps null: false
    end
  end
end
