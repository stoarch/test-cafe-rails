class CreateDrinkDictItems < ActiveRecord::Migration
  def change
    create_table :drink_dict_items do |t|
      t.string :title
      t.decimal :price

      t.timestamps null: false
    end
  end
end
