class AddReferencesToDrink < ActiveRecord::Migration
  def change
    add_reference :drinks, :lineitem, index: true, foreign_key: true
  end
end
