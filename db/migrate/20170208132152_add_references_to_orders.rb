class AddReferencesToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :waitress, index: true, foreign_key: true
  end
end
