json.extract! c_table, :id, :number, :created_at, :updated_at
json.url c_table_url(c_table, format: :json)