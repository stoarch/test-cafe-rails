json.extract! food_dict_item, :id, :title, :price, :created_at, :updated_at
json.url food_dict_item_url(food_dict_item, format: :json)