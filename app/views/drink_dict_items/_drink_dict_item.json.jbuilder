json.extract! drink_dict_item, :id, :title, :price, :created_at, :updated_at
json.url drink_dict_item_url(drink_dict_item, format: :json)