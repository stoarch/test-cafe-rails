json.extract! drink, :id, :title, :price, :created_at, :updated_at
json.url drink_url(drink, format: :json)