json.extract! food, :id, :title, :price, :created_at, :updated_at
json.url food_url(food, format: :json)