json.extract! waitress, :id, :fullname, :created_at, :updated_at
json.url waitress_url(waitress, format: :json)