class DrinkDictItemsController < ApplicationController
  before_action :set_drink_dict_item, only: [:show, :edit, :update, :destroy]

  # GET /drink_dict_items
  # GET /drink_dict_items.json
  def index
    @drink_dict_items = DrinkDictItem.all
  end

  # GET /drink_dict_items/1
  # GET /drink_dict_items/1.json
  def show
  end

  # GET /drink_dict_items/new
  def new
    @drink_dict_item = DrinkDictItem.new
  end

  # GET /drink_dict_items/1/edit
  def edit
  end

  # POST /drink_dict_items
  # POST /drink_dict_items.json
  def create
    @drink_dict_item = DrinkDictItem.new(drink_dict_item_params)

    respond_to do |format|
      if @drink_dict_item.save
        format.html { redirect_to @drink_dict_item, notice: 'Drink dict item was successfully created.' }
        format.json { render :show, status: :created, location: @drink_dict_item }
      else
        format.html { render :new }
        format.json { render json: @drink_dict_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /drink_dict_items/1
  # PATCH/PUT /drink_dict_items/1.json
  def update
    respond_to do |format|
      if @drink_dict_item.update(drink_dict_item_params)
        format.html { redirect_to @drink_dict_item, notice: 'Drink dict item was successfully updated.' }
        format.json { render :show, status: :ok, location: @drink_dict_item }
      else
        format.html { render :edit }
        format.json { render json: @drink_dict_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /drink_dict_items/1
  # DELETE /drink_dict_items/1.json
  def destroy
    @drink_dict_item.destroy
    respond_to do |format|
      format.html { redirect_to drink_dict_items_url, notice: 'Drink dict item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drink_dict_item
      @drink_dict_item = DrinkDictItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drink_dict_item_params
      params.require(:drink_dict_item).permit(:title, :price)
    end
end
