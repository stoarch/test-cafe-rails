class FoodDictItemsController < ApplicationController
  before_action :set_food_dict_item, only: [:show, :edit, :update, :destroy]

  # GET /food_dict_items
  # GET /food_dict_items.json
  def index
    @food_dict_items = FoodDictItem.all
  end

  # GET /food_dict_items/1
  # GET /food_dict_items/1.json
  def show
  end

  # GET /food_dict_items/new
  def new
    @food_dict_item = FoodDictItem.new
  end

  # GET /food_dict_items/1/edit
  def edit
  end

  # POST /food_dict_items
  # POST /food_dict_items.json
  def create
    @food_dict_item = FoodDictItem.new(food_dict_item_params)

    respond_to do |format|
      if @food_dict_item.save
        format.html { redirect_to @food_dict_item, notice: 'Food dict item was successfully created.' }
        format.json { render :show, status: :created, location: @food_dict_item }
      else
        format.html { render :new }
        format.json { render json: @food_dict_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /food_dict_items/1
  # PATCH/PUT /food_dict_items/1.json
  def update
    respond_to do |format|
      if @food_dict_item.update(food_dict_item_params)
        format.html { redirect_to @food_dict_item, notice: 'Food dict item was successfully updated.' }
        format.json { render :show, status: :ok, location: @food_dict_item }
      else
        format.html { render :edit }
        format.json { render json: @food_dict_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /food_dict_items/1
  # DELETE /food_dict_items/1.json
  def destroy
    @food_dict_item.destroy
    respond_to do |format|
      format.html { redirect_to food_dict_items_url, notice: 'Food dict item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_food_dict_item
      @food_dict_item = FoodDictItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def food_dict_item_params
      params.require(:food_dict_item).permit(:title, :price)
    end
end
