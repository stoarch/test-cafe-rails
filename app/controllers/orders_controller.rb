class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
  def index
    get_waitress_id

    unless( @waitress_id.nil? )
      @orders = Order.where( "waitress_id = ?", @waitress_id ).all
    else
      @orders = Order.all
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    get_waitress_id
    return head(:forbidden) if( @waitress_id.nil? )

    @order = Order.new(waitress_id: @waitress_id)
    @food_dict = FoodDictItem.all
    @drink_dict = DrinkDictItem.all
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    line_items_parm = params.permit('lineitems')['lineitems']
    logger.debug line_items_parm

    line_items_codes = line_items_parm.split(':').reject{|l| l.empty? } 

    line_items_codes.each_with_index do |lic, i|
      kind = lic[0] # F - food D - drink
      id = lic[1].to_i

      lineitem = @order.lineitems.build 
      lineitem.number = i.to_s

      if( kind == "F")
        food_item = FoodDictItem.find(id)
        food = Food.new()
        food.lineitem = lineitem
        food.food_dict_item_id = food_item.id
        food.title = food_item.title
        food.price = food_item.price
        food.save

        logger.info food.inspect

        lineitem.kind = Lineitem::FOOD_ITEM
        lineitem.food = food
      end

      if( kind == "D")
        drink_item = DrinkDictItem.find(id)
        drink = Drink.new()
        drink.lineitem = lineitem
        drink.drink_dict_item_id = drink_item.id
        drink.title = drink_item.title
        drink.price = drink_item.price
        drink.save

        logger.info drink.inspect

        lineitem.kind = Lineitem::DRINK_ITEM
        lineitem.drink = drink
      end

      lineitem.save
      
      logger.info lineitem.inspect
    end

    logger.info @order.inspect



    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:number)
    end

    def get_waitress_id
      @waitress_id = params.permit(:waitress_id)[:waitress_id]
    end
end
