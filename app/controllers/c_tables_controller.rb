class CTablesController < ApplicationController
  before_action :set_c_table, only: [:show, :edit, :update, :destroy]

  # GET /c_tables
  # GET /c_tables.json
  def index
    @c_tables = CTable.all
  end

  # GET /c_tables/1
  # GET /c_tables/1.json
  def show
  end

  # GET /c_tables/new
  def new
    @c_table = CTable.new
  end

  # GET /c_tables/1/edit
  def edit
  end

  # POST /c_tables
  # POST /c_tables.json
  def create
    @c_table = CTable.new(c_table_params)

    respond_to do |format|
      if @c_table.save
        format.html { redirect_to @c_table, notice: 'C table was successfully created.' }
        format.json { render :show, status: :created, location: @c_table }
      else
        format.html { render :new }
        format.json { render json: @c_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /c_tables/1
  # PATCH/PUT /c_tables/1.json
  def update
    respond_to do |format|
      if @c_table.update(c_table_params)
        format.html { redirect_to @c_table, notice: 'C table was successfully updated.' }
        format.json { render :show, status: :ok, location: @c_table }
      else
        format.html { render :edit }
        format.json { render json: @c_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /c_tables/1
  # DELETE /c_tables/1.json
  def destroy
    @c_table.destroy
    respond_to do |format|
      format.html { redirect_to c_tables_url, notice: 'C table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_c_table
      @c_table = CTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def c_table_params
      params.require(:c_table).permit(:number)
    end
end
