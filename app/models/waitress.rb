class Waitress < ActiveRecord::Base
  validates :fullname, presence: true
  has_many :orders
end
