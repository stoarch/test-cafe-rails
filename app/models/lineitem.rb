class Lineitem < ActiveRecord::Base
  FOOD_ITEM = 1
  DRINK_ITEM = 2

  validates :number, presence: true
  validates :number, numericality: {greater_than: 0}

  belongs_to :order

  has_one :food
  has_one :drink

  before_destroy :ensure_not_referenced_by_food_or_drink

  private

  def ensure_not_referenced_by_food_or_drink 
    if food.nil? 
      if drink.nil?
        return true
      else
        errors.add(:base, 'drink exists')
        return false
      end
    else
      errors.add(:base, 'food exists')
      return false
    end
  end
end
