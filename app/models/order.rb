class Order < ActiveRecord::Base
  validates :number, presence: true
  belongs_to :waitress

  has_many :lineitems, dependent: :destroy

  before_destroy :ensure_not_referenced_by_any_line_item

  private

  def ensure_not_referenced_by_any_line_item
    if lineitems.empty?
      return true
    else
      errors.add(:base, 'line items exists')
      return false
    end
  end
end
