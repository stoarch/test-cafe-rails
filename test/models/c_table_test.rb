require 'test_helper'

class CTableTest < ActiveSupport::TestCase
  fixtures :c_tables

  test 'c_table attribs must have value' do
    ct = CTable.new

    assert ct.invalid?
    assert ct.errors[:number].any?
  end

  test 'c_table number must be positive' do
    ct = CTable.new(number:0)

    assert ct.invalid?
    assert_equal ["must be greater than or equal to 1"], ct.errors[:number]

    ct.number = 1
    assert ct.valid?

  end
end
