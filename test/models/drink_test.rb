require 'test_helper'

class DrinkTest < ActiveSupport::TestCase

  test 'drink should have data' do
    
    drink = Drink.new

    assert drink.invalid?

    assert drink.errors[:title].any?
    assert drink.errors[:price].any?
  end

  test 'drink price should be positive' do

    drink = Drink.new( title: drinks(:beer).title )

    drink.price = -1
    assert drink.invalid?
    assert_equal ["must be greater than or equal to 0.01"], drink.errors[:price]

    drink.price = 0
    assert drink.invalid?
    assert_equal ["must be greater than or equal to 0.01"], drink.errors[:price]


    drink.price = drinks(:beer).price 

    assert drink.valid?
  end
end
