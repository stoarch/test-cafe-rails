require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  fixtures :clients

  test 'client attribs must be filled' do

    client = Client.new

    assert client.invalid?
    assert client.errors[:fullname].any?
  end

end
