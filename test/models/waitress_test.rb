require 'test_helper'

class WaitressTest < ActiveSupport::TestCase

  test 'waitress data should have values' do

    wt = Waitress.new

    assert wt.invalid?
    assert wt.errors[:fullname].any?
  end
end
