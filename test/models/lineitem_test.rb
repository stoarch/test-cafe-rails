require 'test_helper'

class LineitemTest < ActiveSupport::TestCase

  test 'line item data valid' do

    li = Lineitem.new

    assert li.invalid?

    assert li.errors[:number].any?
  end

  test 'line item number should be positive' do

    li = Lineitem.new

    li.number = -1
    assert li.invalid?
    assert_equal ["must be greater than 0"], li.errors[:number]

    li.number = 0  
    assert li.invalid?
    assert_equal ["must be greater than 0"], li.errors[:number]

    li.number = 1
    assert li.valid?
  end
end
