require 'test_helper'

class DrinkDictItemTest < ActiveSupport::TestCase

  test 'drink dict item should have data' do
    
    drink = DrinkDictItem.new

    assert drink.invalid?

    assert drink.errors[:title].any?
    assert drink.errors[:price].any?
  end

  test 'drink price should be positive' do

    drink = DrinkDictItem.new( title: drinks(:beer).title )

    drink.price = -1
    assert drink.invalid?
    assert_equal ["must be greater than or equal to 0.01"], drink.errors[:price]

    drink.price = 0
    assert drink.invalid?
    assert_equal ["must be greater than or equal to 0.01"], drink.errors[:price]


    drink.price = drinks(:beer).price 

    assert drink.valid?
  end
end
