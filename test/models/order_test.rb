require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test 'order data is valid' do

    order = Order.new

    assert order.invalid?
    assert order.errors[:number].any?
  end

end
