require 'test_helper'

class FoodTest < ActiveSupport::TestCase
  fixtures :foods

  test 'food should have data' do
    
    food = Food.new

    assert food.invalid?

    assert food.errors[:title].any?
    assert food.errors[:price].any?
  end

  test 'food price should be positive' do

    food = Food.new( title: foods(:bigmak).title )

    assert_equal food.title, foods(:bigmak).title

    food.price = -1
    assert food.invalid?
    assert_equal ["must be greater than or equal to 0.01"], food.errors[:price]

    food.price = 0
    assert food.invalid?
    assert_equal ["must be greater than or equal to 0.01"], food.errors[:price]

    food.price = foods(:hotdog).price 

    assert food.valid?
  end
end
