require 'test_helper'

class DrinkDictItemsControllerTest < ActionController::TestCase
  setup do
    @drink_dict_item = drink_dict_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:drink_dict_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create drink_dict_item" do
    assert_difference('DrinkDictItem.count') do
      post :create, drink_dict_item: { price: @drink_dict_item.price, title: @drink_dict_item.title }
    end

    assert_redirected_to drink_dict_item_path(assigns(:drink_dict_item))
  end

  test "should show drink_dict_item" do
    get :show, id: @drink_dict_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @drink_dict_item
    assert_response :success
  end

  test "should update drink_dict_item" do
    patch :update, id: @drink_dict_item, drink_dict_item: { price: @drink_dict_item.price, title: @drink_dict_item.title }
    assert_redirected_to drink_dict_item_path(assigns(:drink_dict_item))
  end

  test "should destroy drink_dict_item" do
    assert_difference('DrinkDictItem.count', -1) do
      delete :destroy, id: @drink_dict_item
    end

    assert_redirected_to drink_dict_items_path
  end
end
