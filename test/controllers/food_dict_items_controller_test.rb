require 'test_helper'

class FoodDictItemsControllerTest < ActionController::TestCase
  setup do
    @food_dict_item = food_dict_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:food_dict_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create food_dict_item" do
    assert_difference('FoodDictItem.count') do
      post :create, food_dict_item: { price: @food_dict_item.price, title: @food_dict_item.title }
    end

    assert_redirected_to food_dict_item_path(assigns(:food_dict_item))
  end

  test "should show food_dict_item" do
    get :show, id: @food_dict_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @food_dict_item
    assert_response :success
  end

  test "should update food_dict_item" do
    patch :update, id: @food_dict_item, food_dict_item: { price: @food_dict_item.price, title: @food_dict_item.title }
    assert_redirected_to food_dict_item_path(assigns(:food_dict_item))
  end

  test "should destroy food_dict_item" do
    assert_difference('FoodDictItem.count', -1) do
      delete :destroy, id: @food_dict_item
    end

    assert_redirected_to food_dict_items_path
  end
end
