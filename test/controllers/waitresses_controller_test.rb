require 'test_helper'

class WaitressesControllerTest < ActionController::TestCase
  setup do
    @waitress = waitresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:waitresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create waitress" do
    assert_difference('Waitress.count') do
      post :create, waitress: { fullname: @waitress.fullname }
    end

    assert_redirected_to waitress_path(assigns(:waitress))
  end

  test "should show waitress" do
    get :show, id: @waitress
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @waitress
    assert_response :success
  end

  test "should update waitress" do
    patch :update, id: @waitress, waitress: { fullname: @waitress.fullname }
    assert_redirected_to waitress_path(assigns(:waitress))
  end

  test "should destroy waitress" do
    assert_difference('Waitress.count', -1) do
      delete :destroy, id: @waitress
    end

    assert_redirected_to waitresses_path
  end
end
