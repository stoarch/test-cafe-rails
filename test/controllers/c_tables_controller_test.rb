require 'test_helper'

class CTablesControllerTest < ActionController::TestCase
  setup do
    @c_table = c_tables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:c_tables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create c_table" do
    assert_difference('CTable.count') do
      post :create, c_table: { number: @c_table.number }
    end

    assert_redirected_to c_table_path(assigns(:c_table))
  end

  test "should show c_table" do
    get :show, id: @c_table
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @c_table
    assert_response :success
  end

  test "should update c_table" do
    patch :update, id: @c_table, c_table: { number: @c_table.number }
    assert_redirected_to c_table_path(assigns(:c_table))
  end

  test "should destroy c_table" do
    assert_difference('CTable.count', -1) do
      delete :destroy, id: @c_table
    end

    assert_redirected_to c_tables_path
  end
end
